
O relatório em PDF, assim como o arquivo executável "main.exe" se encontram na pasta "dist". O código fonte e os casos de teste se encontram na pasta "src".
Para execução do programa deve-se entrar nessa pasta a partir de uma instância do PowerShell ou do Command Prompt do Windows e executá-lo da forma:

.\main.exe 

Assim  que  o  programa  for  inicializado,  estará  aguardando  pelo  caminho  do arquivo de entrada especificado. Exemplo: C:\trab1\src\resources\entrada 1 AFN.txt
Assim que inserido, a execução se inicia e logo que termine será escrito um novo arquivo na mesma pasta chamado "results.txt" e o programa será encerrado automaticamente.