import unittest

from model.automaton import AFD, AFN, parse_transitions
from model.entry import Entries
from utils.utils import read_input_file, read_output_file, write_output_file


class TestUtils(unittest.TestCase):

    def setUp(self):
        self.filename_afd = 'resources/entrada 4.txt'
        self.filename_afn = 'resources/entrada 3 AFN.txt'
        self.filename_results_afd = 'resources/saida 4.txt'
        self.expected_afd = AFD(3, ['a', 'b'], [2], parse_transitions(['0 a 1', '0 b 1', '1 a 1', '1 b 2', '2 a 0', '2 b 2']))
        self.expected_afn = AFN(5, ['a', 'b', 'c'], 2, [3], parse_transitions(['0 a 2', '2 b 2', '2 c 3', '1 b 4', '4 a 4', '4 c 3', '3 a 3', '3 a 4', '3 b 3', '3 b 4']))
        self.expected_entries_afd = ['abbbba', 'aabbbb', 'bbabbabbabbb', 'bbbbbbbbbbb', '-', 'abababababab', 'bbbbaabbbb', 'abba', 'a', 'aaa']
        self.expected_entries_afn = ['abc', 'bac', 'abbbcbabc', 'baacabbcaaac', 'abcabc', 'cab', '-', 'aabc', 'aabbcc', 'babaca']
        self.expected_results_afd = ['rejeita', 'aceita', 'aceita', 'aceita', 'rejeita', 'rejeita', 'aceita', 'rejeita', 'rejeita', 'rejeita']
    
    def test_read_afd(self):
        aut, ent = read_input_file(self.filename_afd)
        self.assertEqual(aut, self.expected_afd)
        self.assertEqual(ent, self.expected_entries_afd)

    def test_read_afn(self):
        aut, ent = read_input_file(self.filename_afn)
        self.assertEqual(aut, self.expected_afn)
        self.assertEqual(ent, self.expected_entries_afn)

    def test_read_output(self):
        outs = read_output_file(self.filename_results_afd)
        self.assertEqual(outs, self.expected_results_afd)
    
    def test_write_output(self):
        outs = read_output_file(self.filename_results_afd)
        write_output_file('output.txt', outs)