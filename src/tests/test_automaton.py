import unittest

from utils.utils import read_input_file, read_output_file

class TestAutomaton(unittest.TestCase):
    def setUp(self):
        self.afd_file = 'resources/entrada 3 AFN.txt'
        self.afd_results_file = 'resources/saida 3 AFN.txt'
        self.aut, self.entries = read_input_file(self.afd_file)
        self.expected_results = read_output_file(self.afd_results_file)
    
    def test_parse_entry(self):
        result = self.aut.parse_entry(self.entries[0])
        self.assertEqual(result, self.expected_results[0])
    
    def test_parse_entries(self):
        results = self.aut.parse(self.entries)
        self.assertEqual(results, self.expected_results)